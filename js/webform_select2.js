(function($) {

  /**
   * The webform select2 behavior.
   */
  Drupal.behaviors.webformSelect2 = {
    attach: function(context, settings) {
      $('select.webform-select-2', context).once('webform-select-2', function() {
        var $select = $(this);

        var options = {
          dropdownParent: $select.closest('.form-item')
        };

        var placeholder = $select.data('webform-select2-placeholder');
        if (placeholder) {
          options.placeholder = placeholder;
        }
        $select.select2(options);
      });
    }
  };

})(jQuery);
